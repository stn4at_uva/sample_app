class CommentsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: :destroy

  def create
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.build(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash[:success] = "Comment posted by user #{User.find(@comment.user_id).name}"
      redirect_to micropost_path(@micropost)
    else
      flash[:error] = "Error with comment"
      redirect_to login_url
    end
  end

  def destroy
    @micropost = Micropost.find(params[:micropost_id])
    @comment = @micropost.comments.find(params[:id])
    @comment.destroy
    redirect_to micropost_path(@micropost)
  end

  private
  
    def comment_params
      params.require(:comment).permit(:user_id, :body)
      #params.require(:comment).permit(:body)
    end

  def correct_user
    @micropost = current_user.microposts.find_by(id: params[:micropost_id])
    redirect_to root_url if @micropost.nil?
  end

end
