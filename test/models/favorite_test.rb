require 'test_helper'

class FavoriteTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @micropost = microposts(:orange)
    @favorite = @user.favorites.build(micropost_id: @micropost.id)
  end

  test "should be valid" do
    assert @favorite.valid?
  end

  test "user id should be required" do
    @favorite.user_id = nil
    assert_not @favorite.valid?
  end

  test "micropost id should be required" do
    @favorite.micropost_id = nil
    assert_not @favorite.valid?
  end


end
