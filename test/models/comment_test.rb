require 'test_helper'

class CommentTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @micropost = microposts(:orange)
    @comment = @micropost.comments.build(body: "Comment 1")
    @comment.user_id = @user.id
  end

  test "should be valid" do
    assert @comment.valid?
  end

  test "user id not nil" do
    assert_not @comment.user_id.nil?
  end

  test "micropost id not nil" do 
    assert_not @comment.micropost_id.nil?
  end

  test "micropost id should be present" do
    @comment.micropost_id = nil
    assert_not @comment.valid?
  end

  test "user id should be present" do
    @comment.user_id = nil
    assert_not @comment.valid?
  end

  test "body should be present" do
    @comment.body = "   "
    assert_not @comment.valid?
  end

  test "body should be at most 200 characters" do
    @comment.body = "a" * 201
    assert_not @comment.valid? 
  end
end
