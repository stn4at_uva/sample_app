require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  def setup
    @micropost = microposts(:orange)
    @comment = comments(:one)
    @comment2 = comments(:two)
    @comment3 = comments(:three)
  end

  test "should redirect comment when not logged in" do
    assert @micropost.id
    assert_no_difference 'Comment.count' do
      post :create, comment: { body: "Comment body" }, micropost_id: @micropost.id
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Comment.count' do
      delete :destroy, id: @comment.id, micropost_id: @comment.micropost_id
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy for wrong user" do
    log_in_as(users(:michael))
    micropost = microposts(:ants)
    assert_no_difference 'Comment.count' do
      delete :destroy, id: @comment.id, micropost_id: @comment.micropost_id
    end
    assert_redirected_to root_url
  end

  test "comment should work" do
    log_in_as(users(:archer))
    assert_difference 'Comment.count', 1 do
      post :create, comment: { body: "Here's a test comment" }, micropost_id: @micropost.id
    end
  end

  # Don't know how to confirm dialog popup in test, so this test
  # currently fails
#  test "user destroy comment on own post" do
#    log_in_as(users(:michael))
#    assert_difference 'Comment.count', -1 do
#      post :destroy, id: @comment2, micropost_id: @comment2.micropost_id
#    end
#    # redirect check?
#  end

end
