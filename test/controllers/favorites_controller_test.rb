require 'test_helper'

class FavoritesControllerTest < ActionController::TestCase
  def setup
    @favorite = favorites(:one)
    @user = users(:michael)
  end

  test "should redirect favorite when not logged in" do
    assert_no_difference 'Favorite.count' do
      post :create, favorite: { micropost_id: microposts(:zone).id }
    end
    assert_redirected_to login_url
  end

### I don't yet follow the syntax for post :create or delete :destroy
  test "favorite should work" do
    log_in_as(@user)
    assert_difference 'Favorite.count', 1 do
      post :create, favorite: { user_id: @user.id}, micropost_id: microposts(:zone).id
    end
  end

end
