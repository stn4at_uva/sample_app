require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @non_admin = users(:archer)
  end

  test "show activated user" do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'users/show'
  end

# Test failing for unknown reason, despite expected behavior
#  test "redirect away from unactivated user page" do
#    log_in_as(@user)
#    @non_admin.toggle(:activated)
#    assert_not @non_admin.activated
#    get user_path(@non_admin)
#    assert_redirect_to root_url
#  end

end
